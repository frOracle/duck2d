package com.wizylab.duck2d;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class Window extends JFrame {
    public static final String KEY = "Window";
    private static final int DEFAULT_WIDTH = 800, DEFAULT_HEIGHT = 600;
    private static final String PROP_TITLE = "window.title";
    private static final String PROP_WIDTH = "window.width";
    private static final String PROP_HEIGHT = "window.height";

    public static Window instance() {
        return Environment.get(KEY);
    }

    public Window() {
        this(Environment.get(PROP_TITLE, ""),
                Environment.get(PROP_WIDTH, DEFAULT_WIDTH),
                Environment.get(PROP_HEIGHT, DEFAULT_HEIGHT));
    }

    public Window(String title, int width, int height) {
        UIManager.put("swing.boldMetal", false);
        getRootPane().setBorder(new EmptyBorder(0, 0, 0, 0));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(new Dimension(width, height));
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle(title);
        // set layout and add panel
        setLayout(new BorderLayout());
        Canvas canvas = new Canvas(width, height);
        add(canvas, BorderLayout.CENTER);
        // add listeners
        Keyboard keyboard = Keyboard.instance();
        if (keyboard != null) addKeyListener(keyboard);
        Mouse mouse = Mouse.instance();
        if (mouse != null) {
            addMouseListener(mouse);
            addMouseMotionListener(mouse);
        }
        // window
        pack();
        center();
    }

    public void center() {
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize(), window = getSize();
        setLocation((screen.width - window.width) / 2, (screen.height - window.height) / 2);
    }
}