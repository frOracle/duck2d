package com.wizylab.duck2d;

import java.util.HashMap;
import java.util.Map;

public class ViewManager {
    public static final String KEY = "ViewManager";
    private Map<Class, View> map = new HashMap<>();
    private Class current;

    public static ViewManager instance() {
        return Environment.get(KEY);
    }

    public static ViewManager add(View... views) {
        ViewManager manager = instance();
        for (View view : views) manager.map.put(view.getClass(), view);
        return manager;
    }

    public static void setCurrent(Class c) {
        View view = ViewManager.getCurrent();
        if (view != null) view.onClose();
        instance().current = c;
        view = ViewManager.getCurrent();
        if (view != null) view.onShow();
    }

    public static View getCurrent() {
        ViewManager manager = instance();
        return manager.map.get(manager.current);
    }
}