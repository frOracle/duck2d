package com.wizylab.duck2d;

import javax.swing.*;
import java.awt.*;

public class Canvas extends JPanel {
    public Canvas(int width, int height) {
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    protected void paintComponent(Graphics graph) {
        Graph g = new Graph(this, graph);
        View view = ViewManager.getCurrent();
        if (view != null) view.onDraw(g);
        g.close();
        revalidate();
        repaint();
    }
}