package com.wizylab.duck2d;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameTimer implements ActionListener {
    public static final String KEY = "GameTimer";
    private Timer timer;
    private long last;

    public static GameTimer instance() {
        return Environment.get(KEY);
    }

    public GameTimer() {
        timer = new Timer(0, this);
        timer.setRepeats(true);
    }

    public void start() {
        last = System.currentTimeMillis();
        timer.start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        long time = System.currentTimeMillis();
        View view = ViewManager.getCurrent();
        if (view != null) view.onTimer(time - this.last);
        this.last = time;
    }
}