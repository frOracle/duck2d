package com.wizylab.duck2d;

import java.util.Random;

public class Game {
    public static final Random RANDOM = new Random();

    public static void start() {
            start((View) null);
    }

    public static void start(View view) {
        start((Object) view);
    }

    public static void start(Class current) {
        start((Object) current);
    }

    private static void start(Object view) {
        if (Drawable.instance() == null) Environment.put(Drawable.KEY, new Drawable());
        if (Fonts.instance() == null) Environment.put(Fonts.KEY, new Fonts());

        if (Keyboard.instance() == null) Environment.put(Keyboard.KEY, new Keyboard());
        if (Mouse.instance() == null) Environment.put(Mouse.KEY, new Mouse());

        if (ViewManager.instance() == null)
            Environment.put(ViewManager.KEY, new ViewManager());

        if (view != null) {
            if (view instanceof View) {
                ViewManager.add((View) view);
                ViewManager.setCurrent(view.getClass());
            } else if (view instanceof Class) {
                ViewManager.setCurrent((Class) view);
            }
        }

        if (Window.instance() == null)
            Environment.put(Window.KEY, new Window());
        Window.instance().setVisible(true);
        Window.instance().requestFocusInWindow();

        if (GameTimer.instance() == null)
            Environment.put(GameTimer.KEY, new GameTimer());
        GameTimer.instance().start();
    }

    public static void addView(View... views) {
        if (ViewManager.instance() == null)
            Environment.put(ViewManager.KEY, new ViewManager());
        ViewManager.add(views);
    }

    public static void show(Class view) {
        ViewManager.setCurrent(view);
    }

    public static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            // nothing
        }
    }
}