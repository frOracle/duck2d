package com.wizylab.duck2d;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.Set;

public class Keyboard implements KeyListener {
    public static final String KEY = "Keyboard";
    private Set<Integer> keys = new HashSet<>();

    public static Keyboard instance() {
        return Environment.get(KEY);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    public static boolean hasKey(int code) {
        return instance().keys.contains(code);
    }

    public static boolean onKey(int code) {
        return instance().keys.remove(code);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        Logger.info(String.valueOf(code));
        keys.add(code);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keys.remove(e.getKeyCode());
    }
}