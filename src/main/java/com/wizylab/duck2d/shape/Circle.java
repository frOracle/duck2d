package com.wizylab.duck2d.shape;

import com.wizylab.duck2d.Collision;

public class Circle {
    public double x, y, r;

    public Circle() {
    }

    public Circle(double x, double y, double r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    public boolean intersects(Circle c) {
        return Collision.collides(this, c);
    }

    public boolean intersects(Rectangle r) {
        return Collision.collides(this, r);
    }

    public boolean contains(double x, double y) {
        double x0 = x - this.x;
        double y0 = y - this.y;
        return x0 * x0 + y0 * y0 <= r * r;
    }
}