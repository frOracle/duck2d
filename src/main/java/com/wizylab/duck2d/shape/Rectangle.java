package com.wizylab.duck2d.shape;

import com.wizylab.duck2d.Collision;


public class Rectangle {
    public double x, y, width, height;

    public Rectangle() {
    }

    public Rectangle(double x, double y, double width, double height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public boolean intersects(Rectangle r) {
        return Collision.collides(this, r);
    }

    public boolean intersects(Circle c) {
        return Collision.collides(c, this);
    }

    public boolean contains(double x, double y) {
        return this.x <= x && x <= this.x + width && this.y <= y && y <= this.y + height;
    }
}