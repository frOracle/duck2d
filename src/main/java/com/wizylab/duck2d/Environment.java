package com.wizylab.duck2d;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

@SuppressWarnings({"unchecked"})
public class Environment {
    private static final String DOUBLE = "^[-+]?\\d*\\.?\\d+$";
    private static final String INTEGER = "^[-+]?\\d+$";
    private static final Map<String, Object> map = new HashMap<>();

    public static void put(File file) {
        try (Scanner sc = new Scanner(file)) {
            while (sc.hasNextLine()) {
                String str = sc.nextLine().trim();
                int i = str.indexOf('=');
                if (str.length() == 0 || str.charAt(0) == '#' || i < 0) continue;
                String key = str.substring(0, i).trim(), value = str.substring(i + 1).trim();
                if (value.matches(INTEGER)) map.put(key, Integer.parseInt(value));
                else if (value.matches(DOUBLE)) map.put(key, Double.parseDouble(value));
                else map.put(key, value);
            }
            Logger.info("Config file loaded");
        } catch (Exception e) {
            Logger.error("Can't load config file" + e.getMessage());
        }
    }

    public static void put(String key, Object obj) {
        map.put(key, obj);
    }

    public static <T> T get(String key) {
        return get(key, null);
    }

    public static <T> T get(String key, T def) {
        try {
            T value = (T) map.get(key);
            return (value != null) ? value : def;
        } catch (ClassCastException e) {
            return def;
        }
    }

    public static void clear() {
        map.clear();
    }
}