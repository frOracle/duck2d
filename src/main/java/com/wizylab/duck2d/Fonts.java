package com.wizylab.duck2d;

import java.awt.*;
import java.io.File;
import java.util.HashMap;

public class Fonts {
    public static final String KEY = "Fonts";
    private static final String DELIMITER = "/";
    private static final String DEFAULT_PATH = "fonts/";
    private static final String PROP_PATH = "fonts.path";
    private final HashMap<String, Font> data = new HashMap<>();

    public static Fonts instance() {
        return Environment.get(KEY);
    }

    public Fonts() {
        try {
            String path = Environment.get(PROP_PATH, DEFAULT_PATH);
            File dir = new File(path);
            if (!dir.exists()) throw new Exception("Fonts dir not found (" + path + ")");
            load(dir, "");
        } catch (Exception e) {
            Logger.info(e.getMessage());
        }
    }

    public static Font get(String key) {
        return instance().data.get(key.toLowerCase());
    }

    public static Font get(String key, int style) {
        Font font = instance().data.get(key.toLowerCase());
        return (font != null) ? font.deriveFont(style) : null;
    }

    public static Font get(String key, float size) {
        Font font = instance().data.get(key.toLowerCase());
        return (font != null) ? font.deriveFont(size) : null;
    }

    public static Font get(String key, int style, float size) {
        Font font = instance().data.get(key.toLowerCase());
        return (font != null) ? font.deriveFont(style, size) : null;
    }

    private void load(File dir, String prefix) {
        for (File f : dir.listFiles()) {
            try {
                if (f.isFile()) {
                    String name = f.getName().replaceFirst("[.][^.]+$", "");
                    String key = (prefix + name).toLowerCase();
                    data.put(key, Font.createFont(Font.TRUETYPE_FONT, f));
                    Logger.info("Font add successfully - " + key);
                } else {
                    load(f, prefix + f.getName() + DELIMITER);
                }
            } catch (Exception e) {
                Logger.error("Invalid drawable resource - " + e.getMessage());
            }
        }
    }
}