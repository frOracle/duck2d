package com.wizylab.duck2d;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashSet;
import java.util.Set;

public class Mouse implements MouseListener, MouseMotionListener {
    public static final String KEY = "Mouse";
    private Set<Integer> buttons = new HashSet<>();
    private Point position = new Point();

    public static Mouse instance() {
        return Environment.get(KEY);
    }

    public static int x() {
        return instance().position.x;
    }

    public static int y() {
        return instance().position.y;
    }

    public static boolean hasClick(int btn) {
        return instance().buttons.contains(btn);
    }

    public static boolean onClick(int btn) {
        return instance().buttons.remove(btn);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        buttons.add(e.getButton());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        buttons.remove(e.getButton());
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        position.setLocation(e.getX() - 3, e.getY() - 24);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        position.setLocation(e.getX() - 3, e.getY() - 24);
    }
}