package com.wizylab.duck2d;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;

@SuppressWarnings("ConstantConditions")
public class Drawable {
    public static final String KEY = "Drawable";
    private static final String DELIMITER = "/";
    private static final String DEFAULT_PATH = "assets/";
    private static final String PROP_PATH = "drawable.path";
    private final HashMap<String, BufferedImage> data = new HashMap<>();

    public static Drawable instance() {
        return Environment.get(KEY);
    }

    public Drawable() {
        try {
            String path = Environment.get(PROP_PATH, DEFAULT_PATH);
            File dir = new File(path);
            if (!dir.exists()) throw new Exception("Drawable dir not found (" + path + ")");
            load(dir, "");
        } catch (Exception e) {
            Logger.info(e.getMessage());
        }
    }

    public static BufferedImage get(String key) {
        return instance().data.get(key.toLowerCase());
    }

    public static BufferedImage get(String key, int width, int height) {
        final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = image.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.drawImage(instance().data.get(key.toLowerCase()), 0, 0, width, height, null);
        g.dispose();
        return image;
    }

    private void load(File dir, String prefix) {
        for (File f : dir.listFiles()) {
            try {
                if (f.isFile()) {
                    String name = f.getName().replaceFirst("[.][^.]+$", "");
                    String key = (prefix + name).toLowerCase();
                    BufferedImage img = ImageIO.read(new FileInputStream(f));
                    data.put(key, img);
                    Logger.info("Drawable add successfully - " + key);
                } else {
                    load(f, prefix + f.getName() + DELIMITER);
                }
            } catch (Exception e) {
                Logger.error("Invalid drawable resource - " + e.getMessage());
            }
        }
    }
}