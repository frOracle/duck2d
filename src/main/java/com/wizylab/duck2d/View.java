package com.wizylab.duck2d;

public interface View {
    default void onShow() {
    }

    default void onClose() {
    }

    void onTimer(long interval);

    void onDraw(Graph g);
}