package com.wizylab.duck2d;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.image.BufferedImage;
import java.io.Closeable;

public class Graph implements Closeable {
    private static final String[] FONTS = {
            "Arial", "Arial Black", "MS Serif", "MS Sans Serif", "Comic Sans MS",
            "Tahoma", "Times New Roman", "Impact", "Verdana", "Franklin Gothic Medium",
            "Gautami", "Georgia", "Latha", "Lucida Sans Unicode", "Courier New", "Lucida Console"};
    private static final Font DEFAULT_FONT = new Font(FONTS[0], Font.PLAIN, 20);
    private BufferedImage image = null;
    private Point to = new Point();
    private JPanel parent = null;
    private Graphics2D g;
    private Color fColor;

    public Graph(int width, int height) {
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        g = prepare(image.createGraphics());
    }

    public Graph(JPanel parent, Graphics graphics) {
        g = prepare((Graphics2D) graphics);
        this.parent = parent;
        setBackground(Color.BLACK);
    }

    public FontMetrics getFontMetrics(Font font) {
        return g.getFontMetrics(font);
    }

    public void setColor(Color color) {
        g.setColor(color);
    }

    public void setFillColor(Color color) {
        fColor = color;
    }

    public void setStroke(int width) {
        g.setStroke(new BasicStroke(width));
    }

    public void setBackground(Color color) {
        Color tmp = g.getColor();
        g.setColor(color);
        int width = (parent == null) ? image.getWidth() : parent.getWidth();
        int height = (parent == null) ? image.getHeight() : parent.getHeight();
        g.fillRect(0, 0, width, height);
        g.setColor(tmp);
    }

    public void putPixel(int x, int y) {
        g.drawLine(x, y, x, y);
    }

    public void moveTo(int x, int y) {
        to.setLocation(x, y);
    }

    public void lineTo(int x, int y) {
        line(to.x, to.y, x, y);
    }

    public void line(int x1, int y1, int x2, int y2) {
        g.drawLine(x1, y1, x2, y2);
        to.setLocation(x2, y2);
    }

    public void curve(int x, int y, int rx, int ry, int as, int ae) {
        g.draw(new Arc2D.Double(x, y, rx, ry, as, ae - as, Arc2D.OPEN));
    }

    public void rect(int x1, int y1, int x2, int y2) {
        g.drawRect(x1, y1, x2 - x1, y2 - y1);
    }

    public void rect(int x1, int y1, int x2, int y2, int angle) {
        g.drawRoundRect(x1, y1, x2 - x1, y2 - y1, angle, angle);
    }

    public void fillRect(com.wizylab.duck2d.shape.Rectangle r) {
        Color tmp = g.getColor();
        g.setColor(fColor);
        g.fillRoundRect((int) r.x, (int) r.y, (int) r.width, (int) r.height, 0, 0);
        g.setColor(tmp);
    }

    public void fillRect(double x1, double y1, double x2, double y2) {
        fillRect(x1, y1, x2, y2, 0);
    }

    public void fillRect(double x1, double y1, double x2, double y2, int angle) {
        Color tmp = g.getColor();
        g.setColor(fColor);
        g.fillRoundRect((int) x1, (int) y1, (int) (x2 - x1), (int) (y2 - y1), angle, angle);
        g.setColor(tmp);
    }

    public void circle(com.wizylab.duck2d.shape.Circle c) {
        circle(c.x, c.y, c.r);
    }

    public void circle(double x, double y, double r) {
        ellipse((int) x, (int) y, (int) r, (int) r);
    }

    public void fillCircle(com.wizylab.duck2d.shape.Circle c) {
        fillCircle(c.x, c.y, c.r);
    }

    public void fillCircle(double x, double y, double r) {
        fillEllipse((int) x, (int) y, (int) r, (int) r);
    }

    public void ellipse(int x, int y, int rx, int ry) {
        g.drawOval(x - rx, y - ry, rx * 2, ry * 2);
    }

    public void fillEllipse(int x, int y, int rx, int ry) {
        Color tmp = g.getColor();
        g.setColor(fColor);
        g.fillOval(x - rx, y - ry, rx * 2, ry * 2);
        g.setColor(tmp);
    }

    public void setFont(Font font) {
        g.setFont(font);
    }

    public void setTextStyle(int font, int style, int size) {
        g.setFont(new Font(FONTS[font % FONTS.length], style, size));
    }

    public void text(int x, int y, String text) {
//        FontMetrics metrics = g.getFontMetrics(g.getFont());
//        g.drawString(text, x, y+metrics.getHeight() / 2);
        g.drawString(text, x, y);
    }

    public void ctext(int x1, int y1, int x2, int y2, String text) {
        ctext(new com.wizylab.duck2d.shape.Rectangle(x1, y1, x2 - x1, y2 - y1), text);
    }

    public void ctext(com.wizylab.duck2d.shape.Rectangle rect, String text) {
        FontMetrics metrics = g.getFontMetrics(g.getFont());
        double x = rect.x + (rect.width - metrics.stringWidth(text)) / 2;
        double y = rect.y + ((rect.height - metrics.getHeight()) / 2) + metrics.getAscent();
        g.drawString(text, (int) x, (int) y);
    }

    public void putGraph(Graph g) {
        if (g.image != null) this.g.drawImage(g.image, 0, 0, g.image.getWidth(), g.image.getHeight(), null);
    }

    public void putGraph(Graph g, int x1, int y1, int x2, int y2, int sx1, int sy1, int sx2, int sy2) {
        if (g.image != null) this.g.drawImage(g.image, x1, y1, x2, y2, sx1, sy1, sx2, sy2, null);
    }

    public void putImage(String texture, double x, double y) {
        putImage(texture, (int) x, (int) y, -1, -1);
    }

    public void putImage(String texture, double x, double y, int size) {
        putImage(texture, (int) x, (int) y, size, size);
    }

    public void putImage(String texture, double x, double y, int width, int height) {
        putImage(texture, (int) x, (int) y, width, height);
    }

    public void putImage(String texture, int x, int y) {
        putImage(texture, x, y, -1, -1);
    }

    public void putImage(String texture, int x, int y, int size) {
        putImage(texture, x, y, size, size);
    }

    public void putImage(String texture, int x, int y, int width, int height) {
        BufferedImage image = Drawable.get(texture);
        if (image == null) return;
        if (width < 0) width = image.getWidth();
        if (height < 0) height = image.getHeight();
        g.drawImage(image, x, y, width, height, null);
    }

    public void putRotateImage(String texture, double angle, double x, double y) {
        putRotateImage(texture, angle, (int) x, (int) y, -1, -1);
    }

    public void putRotateImage(String texture, double angle, double x, double y, int size) {
        putRotateImage(texture, angle, (int) x, (int) y, size, size);
    }

    public void putRotateImage(String texture, double angle, double x, double y, int width, int height) {
        putRotateImage(texture, angle, (int) x, (int) y, width, height);
    }

    public void putRotateImage(String texture, double angle, int x, int y) {
        putRotateImage(texture, angle, x, y, -1, -1);
    }

    public void putRotateImage(String texture, double angle, int x, int y, int size) {
        putRotateImage(texture, angle, x, y, size, size);
    }

    public void putRotateImage(String texture, double angle, int x, int y, int width, int height) {
        BufferedImage image = Drawable.get(texture);
        if (image == null) return;

        int oWidth = image.getWidth(), oHeight = image.getHeight();
        if (width < 0) width = oWidth;
        if (height < 0) height = oHeight;

        AffineTransform at = new AffineTransform();
        at.translate(-oWidth / 2, -oHeight / 2);
        at.translate(x + oWidth / 2, y + oHeight / 2);
        at.scale((double) width / oWidth, (double) height / oHeight);
        at.rotate(angle, oWidth / 2, oHeight / 2);
        g.drawImage(image, at, null);
    }

    @Override
    public void close() {
        g.finalize();
    }

    private Graphics2D prepare(Graphics2D g) {
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setFont(DEFAULT_FONT);
        return g;
    }
}