package com.wizylab.duck2d;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.HashSet;
import java.util.Random;

@Deprecated
public abstract class SimpleGame extends JFrame implements ActionListener, KeyListener, MouseListener, MouseMotionListener {
    protected static final Random RANDOM = new Random();
    private HashSet<Integer> keys;
    private Timer timer;
    private long last;

    public SimpleGame() {
        this("", 800, 600);
    }

    public SimpleGame(String title, int width, int height) {
        UIManager.put("swing.boldMetal", false);
        getRootPane().setBorder(new EmptyBorder(0, 0, 0, 0));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle(title);
        // set layout and add panel
        setLayout(new BorderLayout());
        add(new Canvas(width, height), BorderLayout.CENTER);
        // add listeners
        addKeyListener(this);
        addMouseListener(this);
        addMouseMotionListener(this);
        // create keys set and timer
        keys = new HashSet<>();
        timer = new Timer(1, this);
        timer.setRepeats(true);
        // init sprites
        Environment.put(Drawable.KEY, new Drawable());
        // window
        pack();
        center();
    }

    public void start() {
        last = System.currentTimeMillis();
        timer.start();
        setVisible(true);
        requestFocusInWindow();
    }

    public boolean onKey(int code) {
        return keys.contains(code);
    }

    public abstract void onTimer(long interval);

    public abstract void onDraw(Graph g);

    private void center() {
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize(), window = getSize();
        setLocation((screen.width - window.width) / 2, (screen.height - window.height) / 2);
    }

    private class Canvas extends JPanel {
        private Canvas(int width, int height) {
            setPreferredSize(new Dimension(width, height));
        }

        @Override
        protected void paintComponent(Graphics graph) {
            Graph g = new Graph(this, graph);
            onDraw(g);
            g.close();
            revalidate();
            repaint();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        long time = System.currentTimeMillis();
        onTimer(time - this.last);
        this.last = time;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        Logger.info(String.valueOf(code));
        keys.add(code);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keys.remove(e.getKeyCode());
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }
}