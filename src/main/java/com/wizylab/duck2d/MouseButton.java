package com.wizylab.duck2d;

import java.awt.event.MouseEvent;

public class MouseButton {
    public static final int LEFT = MouseEvent.BUTTON1;
    public static final int CENTER = MouseEvent.BUTTON2;
    public static final int RIGHT = MouseEvent.BUTTON3;
}