package com.wizylab.duck2d;

import com.wizylab.duck2d.shape.Circle;
import com.wizylab.duck2d.shape.Rectangle;

public class Collision {
    public static boolean collides(Rectangle r1, Rectangle r2) {
        if (r1 == null || r2 == null) return false;
        return (r1.x <= r2.x + r2.width && r1.x + r1.width >= r2.x && r1.y <= r2.y + r2.height && r1.height + r1.y >= r2.y);
    }

    public static boolean collides(Circle c1, Circle c2) {
        if (c1 == null || c2 == null) return false;
        return Math.pow(c1.x - c2.x, 2) + Math.pow(c1.y - c2.y, 2) <= Math.pow(c1.r + c2.r, 2);
    }

    public static boolean collides(Circle c, Rectangle r) {
        if (c == null || r == null) return false;
        double closestX = clamp(c.x, r.x, r.x + r.width);
        double closestY = clamp(c.y, r.y, r.y + r.height);
        double distanceX = c.x - closestX;
        double distanceY = c.y - closestY;
        return Math.pow(distanceX, 2) + Math.pow(distanceY, 2) <= Math.pow(c.r, 2);
    }

    private static double clamp(double value, double min, double max) {
        if (value < min) return min;
        else if (value > max) return max;
        return value;
    }
}