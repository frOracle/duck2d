package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.*;

import java.awt.*;

import static com.wizylab.duck2d.Keyboard.onKey;
import static java.awt.event.KeyEvent.VK_ESCAPE;

public class Rotate3 implements View {
    private static final Rectangle WINDOW_SQUARE = new Rectangle(0, 0, 800, 600);
    private static final float SPEED = 0.2F;
    private double x = 100, y = 100, angle = 0;
    private Bullet[] bullets = new Bullet[100];

    public static void main(String[] args) {
        Game.start(new Rotate3());
    }

    private Rotate3() {
        for (int i = 0; i < bullets.length; i++) bullets[i] = new Bullet();
    }

    @Override
    public void onTimer(long interval) {
        if (onKey(VK_ESCAPE)) System.exit(0);
        // смащаем
        double x = this.x - Mouse.x();
        double y = this.y - Mouse.y();
        // в цилиндрические
        double distance = Math.sqrt(x * x + y * y);
        double angle = Math.atan2(y, x);
        // перемещаем
        distance -= SPEED * interval;
        if (distance > 1) {
            // переводим обратно
            x = distance * Math.cos(angle);
            y = distance * Math.sin(angle);
            // устанавливаем новые значения
            this.angle = angle - Math.PI / 2;
            this.x = x + Mouse.x();
            this.y = y + Mouse.y();
        }

        if (Mouse.onClick(MouseButton.LEFT)) {
            for (Bullet bullet : bullets) {
                if (!bullet.isFree()) continue;
                bullet.set(this.x, this.y, angle + Math.PI);
                break;
            }
        }

        for (Bullet bullet : bullets) bullet.move();
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("raptor/bg", 0, 0, 800, 600);
        g.putRotateImage("raptor/ships/9", angle, x - 25, y - 25, 50);
        for (Bullet b : bullets)
            g.putRotateImage("raptor/rockets/0", b.angle + Math.PI / 2, b.x - 16, b.y - 16, 32);
    }

    private class Bullet {
        private static final float BOLET_SPEED = 10;
        private double x, y, angle;

        private Bullet() {
            reset();
        }

        private void set(double x, double y, double angle) {
            this.x = x;
            this.y = y;
            this.angle = angle;
        }

        private void move() {
            if (isFree()) return;
            x += BOLET_SPEED * Math.cos(angle);
            y += BOLET_SPEED * Math.sin(angle);
            if (!WINDOW_SQUARE.contains(x, y)) reset();
        }

        private void reset() {
            x = y = angle = -1;
        }

        private boolean isFree() {
            return x < 0;
        }
    }
}