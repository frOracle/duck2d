package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static com.wizylab.duck2d.Keyboard.hasKey;
import static com.wizylab.duck2d.Keyboard.onKey;
import static java.awt.event.KeyEvent.*;

public class Maze2 implements View {
    private static final int TX_PLAYER = 3;
    private static final int SPEED = 5;
    private int x = 100, y = 100;
    private int walls[][];
    private int level = 1;
    private int state = 0;
    private float frame = 0;
    private int a;
    private int a1;

    private Maze2() {
        load(level);
    }

    public static void main(String[] args) {
        Game.start(new Maze2());
    }

    @Override
    public void onTimer(long l) {
        if (onKey(VK_ESCAPE)) System.exit(0);
        if (hasKey(VK_ENTER)) load(++level);
        if (hasKey(VK_LEFT) && !isWall(x - SPEED, y)) x -= SPEED;
        if (hasKey(VK_RIGHT) && !isWall(x + SPEED, y)) x += SPEED;
        if (hasKey(VK_UP) && !isWall(x, y - SPEED)) y -= SPEED;
        if (hasKey(VK_DOWN) && !isWall(x, y + SPEED)) y += SPEED;
        frame += 0.5;
        if (frame > 15) frame = 0;
    }

    private void load(int level) {
        try {
            if (level > 2) {
                state = 1;
                return;
            }
            Scanner sc = new Scanner(new File("data/maze/level" + level + ".txt"));
            walls = new int[sc.nextInt()][4];
            for (int i = 0; i < walls.length; i++)
                walls[i] = new int[]{sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt()};
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private boolean isWall(int x, int y) {
        Rectangle rPlayer = new Rectangle(x - 20, y - 20, 40, 40);
        for (int[] wall : walls) {
            int w = wall[2] - wall[0], h = wall[3] - wall[1];
            if (rPlayer.intersects(wall[0], wall[1], w, h)) return true;
        }
        return false;
    }

    @Override
    public void onDraw(Graph g) {
        if (state == 0) {
            g.putImage("raptor/bg", 0, 0, 800, 600);
            g.setFillColor(Color.YELLOW);
            for (int[] wall : walls)
                g.putImage("snow/bg", wall[0], wall[1], wall[2]-wall[0], wall[3]-wall[1]);

            g.putImage("raptor/rocks/" + TX_PLAYER + "/" + (int) frame, x - 20, y - 20, 40);
        } else if (state == 1) {
            g.setColor(Color.YELLOW);
            g.setTextStyle(1, 2, 80);
            g.text(120, 270, "GAME OVER");
        }
    }
}