package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import java.awt.*;

import static com.wizylab.duck2d.Game.RANDOM;
import static com.wizylab.duck2d.Keyboard.onKey;
import static java.awt.event.KeyEvent.VK_ESCAPE;

public class Stars implements View {
    private int[][] data = new int[500][2];

    public static void main(String[] args) {
        Game.start(new Stars());
    }

    private Stars() {
        for (int i = 0; i < data.length; i++) {
            data[i][0] = RANDOM.nextInt(800);
            data[i][1] = RANDOM.nextInt(600);
        }
    }

    @Override
    public void onTimer(long l) {
        if (onKey(VK_ESCAPE)) System.exit(0);
    }

    @Override
    public void onDraw(Graph g) {
        g.setColor(Color.WHITE);
        for (int[] p : data) g.putPixel(p[0], p[1]);
    }
}