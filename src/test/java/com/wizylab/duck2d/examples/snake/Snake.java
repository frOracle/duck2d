package com.wizylab.duck2d.examples.snake;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import java.awt.*;

import static com.wizylab.duck2d.Game.RANDOM;
import static com.wizylab.duck2d.Keyboard.onKey;
import static com.wizylab.duck2d.examples.snake.Map.*;
import static java.awt.event.KeyEvent.*;

public class Snake implements View {
    private static final String[] NAMES = {"", "brick", "apple", "life"};
    private static final int CELL_SIZE = 25;
    private int result = 0, level = 0, life = 3, score = 0;
    private int[][] map = MAPS[level];
    private int sTime = 0, aTime = 0;
    private int[][] snake;
    private int dir;

    public static void main(String[] args) {
        Game.start(new Snake());
    }

    private Snake() {
        resetApples();

        reset();
    }

    @Override
    public void onTimer(long interval) {
        if (onKey(VK_ESCAPE)) System.exit(0);

        if (result != 0) {
            if (onKey(VK_ENTER)) result = 0;
            return;
        }

        if (onKey(VK_LEFT)) dir = (dir - 1 >= 0) ? dir - 1 : 3;
        if (onKey(VK_RIGHT)) dir = (dir + 1 <= 3) ? dir + 1 : 0;
        // add apple time
        aTime += interval;
        if (aTime > APPLE_INTERVAL[level]) {
            aTime = 0;
            resetApples();
        }
        // add tic time
        sTime += interval;
        if (sTime <= PLAYER_INTERVAL[level]) return;
        sTime = 0;

        // move snake
        int tx = snake[snake.length - 1][0], ty = snake[snake.length - 1][1];
        for (int i = snake.length - 1; i > 0; i--) {
            snake[i][0] = snake[i - 1][0];
            snake[i][1] = snake[i - 1][1];
        }
        if (dir == 0) snake[0][1]--;
        if (dir == 1) snake[0][0]++;
        if (dir == 2) snake[0][1]++;
        if (dir == 3) snake[0][0]--;

        int x = snake[0][1], y = snake[0][0], cell = map[x][y];
        // find across himself
        boolean across = false;
        for (int i = 1; i < snake.length; i++)
            if (x == snake[i][1] && y == snake[i][0]) across = true;
        // check
        if (across || cell == 1) {
            life--;
            reset();
            resetApples();
            // game over
            if (life < 1) {
                result = -1;
                life = 0;
            }
        } else if (cell == 2) {
            map[x][y] = 0;
            score++;
            // add new node to snake
            int[][] tmp = snake;
            snake = new int[tmp.length + 1][2];
            System.arraycopy(tmp, 0, snake, 0, tmp.length);
            snake[snake.length - 1][0] = tx;
            snake[snake.length - 1][1] = ty;
            if (score >= LEVEL_SCORE[level]) {
                level++;
                result = 1;
                if (level >= MAPS.length) {
                    result = 2;
                } else map = MAPS[level];
                reset();
                resetApples();
                score = 0;
            }
        } else if (cell == 3) {
            map[x][y] = 0;
            life++;
        }
    }

    @Override
    public void onDraw(Graph g) {
        // draw map
        for (int x = 0; x < 32; x++)
            for (int y = 0; y < 24; y++)
                if (map[y][x] != 0)
                    g.putImage("snake/" + NAMES[map[y][x]], x * CELL_SIZE + 1, y * CELL_SIZE + 1, CELL_SIZE - 1);
        for (int[] node : snake) {
            g.setFillColor(Color.ORANGE);
            g.fillCircle(node[0] * CELL_SIZE + CELL_SIZE / 2, node[1] * CELL_SIZE + CELL_SIZE / 2, CELL_SIZE / 2 - 1);
            g.setFillColor(Color.YELLOW);
            g.fillCircle(node[0] * CELL_SIZE + CELL_SIZE / 2, node[1] * CELL_SIZE + CELL_SIZE / 2, CELL_SIZE / 3);
        }
        // print score
        g.setTextStyle(7, 1, 20);
        g.setColor(Color.WHITE);
        g.text(10, 25, "SCORE: " + score);
        g.text(10, 48, "LIFE: " + life);
        // check end game
        if (result == 0) return;
        g.setTextStyle(0, 1, 80);
        if (result == -1) {
            g.setColor(Color.RED);
            g.text(150, 290, "GAME OVER");
        } else if (result == 2) {
            g.setColor(Color.BLUE);
            g.text(200, 290, "YOU WIN!");
        } else {
            g.setColor(Color.YELLOW);
            g.text(230, 290, "LEVEL " + level);
            g.setTextStyle(7, 1, 14);
            g.text(300, 310, "PRESS ENTER TO CONTINUE");
        }
    }

    private void reset() {
        dir = 1;
        sTime = aTime = 0;
        snake = new int[3][2];
        for (int i = 0; i < snake.length; i++) {
            snake[i][0] = 5 + snake.length - i - 1;
            snake[i][1] = 5;
        }
    }

    private void resetApples() {
        for (int x = 0; x < 32; x++)
            for (int y = 0; y < 24; y++)
                if (map[y][x] == 2 || map[y][x] == 3) map[y][x] = 0;
        int i = 0;
        while (i < 5) {
            int x = RANDOM.nextInt(32);
            int y = RANDOM.nextInt(24);
            if (map[y][x] != 0) continue;
            map[y][x] = (RANDOM.nextInt(100) < 5) ? 3 : 2;
            i++;
        }
    }
}