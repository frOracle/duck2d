package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import java.awt.*;

import static com.wizylab.duck2d.Keyboard.hasKey;
import static com.wizylab.duck2d.Keyboard.onKey;
import static java.awt.event.KeyEvent.*;

@SuppressWarnings("Duplicates")
public class Maze1 implements View {
    private static final float SPEED = 0.3F;
    private static final int TIME = 30000;
    private static final String[] NAMES = {"", "brick", "apple", "door"};
    private static final int[][] MAP = {
            new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            new int[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            new int[]{1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1},
            new int[]{1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1},
            new int[]{1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1},
            new int[]{1, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1},
            new int[]{1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            new int[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            new int[]{1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            new int[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 1},
            new int[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
            new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
    private int score = 0, time = TIME, result = 0;
    private float x = 50, y = 50;

    public static void main(String[] args) {
        Game.start(new Maze1());
    }

    @Override
    public void onTimer(long interval) {
        if (onKey(VK_ESCAPE)) System.exit(0);
        if (result != 0) return;
        // time
        time -= interval;
        if (time < 0) {
            result = -1;
            time = 0;
        }
        // move
        float speed = interval * SPEED;
        if (hasKey(VK_LEFT) && (!isCollision(x - speed, y))) x -= speed;
        if (hasKey(VK_UP) && (!isCollision(x, y - speed))) y -= speed;
        if (hasKey(VK_RIGHT) && (!isCollision(x + speed, y))) x += speed;
        if (hasKey(VK_DOWN) && (!isCollision(x, y + speed))) y += speed;
    }

    @Override
    public void onDraw(Graph g) {
        // draw game objects
        for (int x = 0; x < 16; x++)
            for (int y = 0; y < 12; y++)
                if (MAP[y][x] != 0) g.putImage("maze/" + NAMES[MAP[y][x]], x * 50 + 1, y * 50 + 1, 48);
        g.putImage("maze/player", (int) x, (int) y, 50);
        // draw time panel
        g.putImage("maze/line", 10, 10, 200, 30);
        g.putImage("maze/fline", 12, 12, 196 * time / TIME, 26);
        // print score
        g.setTextStyle(0, 1, 30);
        g.setColor(Color.WHITE);
        g.text(620, 35, "SCORE: " + score);
        // check end game
        if (result == 0) return;
        g.setTextStyle(0, 1, 80);
        if (result == -1) {
            g.setColor(Color.RED);
            g.text(150, 300, "GAME OVER");
        } else {
            g.setColor(Color.BLUE);
            g.text(200, 300, "YOU WIN!");
        }
    }

    private boolean isCollision(float x, float y) {
        int size = 50, gap = 3;
        int x1 = (int) ((x + gap) / size), x2 = (int) ((x + size - 2 * gap) / size);
        int y1 = (int) ((y + gap) / size), y2 = (int) ((y + size - 2 * gap) / size);
        int[][] points = {{x1, y1}, {x2, y1}, {x1, y2}, {x2, y2}};
        for (int[] p : points) {
            double distance = distance(x, y, p[0] * 50, p[1] * 50);
            int cell = MAP[p[1]][p[0]];
            if (cell == 1) {
                return true;
            } else if (cell == 2 && distance < 20) {
                MAP[p[1]][p[0]] = 0;
                score++;
            } else if (cell == 3 && distance < 20) {
                result = 1;
            }
        }
        return false;
    }

    private static double distance(double x1, double y1, double x2, double y2) {
        double x = x2 - x1, y = y2 - y1;
        return Math.sqrt(x * x + y * y);
    }
}