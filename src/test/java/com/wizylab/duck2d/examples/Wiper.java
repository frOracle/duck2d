package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.Keyboard;
import com.wizylab.duck2d.View;

import java.awt.*;
import java.awt.event.KeyEvent;

public class Wiper implements View {
    private static Graph graph = new Graph(800, 600);
    private static long TIME = 1;

    public static void main(String[] args) {
        Game.start(new Wiper());

        while (true) {
            // TASK 1
            graph.setColor(Color.GREEN);
            for (int i = 0; i < 800; i++) {
                graph.line(i, 0, i, 600);
                Game.sleep(TIME);
            }
            graph.setColor(Color.BLUE);
            for (int i = 800; i >= 0; i--) {
                graph.line(i, 0, i, 600);
                Game.sleep(TIME);
            }

            // TASK 2
            graph.setColor(Color.MAGENTA);
            for (int i = 0; i < 800; i++) {
                graph.line(0, 600, i, 0);
                Game.sleep(TIME);
            }
            for (int i = 0; i < 600; i++) {
                graph.line(0, 600, 800, i);
                Game.sleep(TIME);
            }

            // TASK 3
            graph.setColor(Color.YELLOW);
            for (int i = 800; i >= 0; i--) {
                graph.line(800, 600, i, 0);
                Game.sleep(TIME);
            }
            for (int i = 0; i < 600; i++) {
                graph.line(800, 600, 0, i);
                Game.sleep(TIME);
            }

            // TASK 4
            graph.setColor(Color.RED);
            for (int i = 600; i >= 0; i--) {
                graph.line(400, 600, 0, i);
                Game.sleep(TIME);
            }
            for (int i = 0; i < 800; i++) {
                graph.line(400, 600, i, 0);
                Game.sleep(TIME);
            }
            for (int i = 0; i < 600; i++) {
                graph.line(400, 600, 800, i);
                Game.sleep(TIME);
            }
        }
    }

    @Override
    public void onTimer(long interval) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);
    }

    @Override
    public void onDraw(Graph g) {
        g.putGraph(graph);
    }
}