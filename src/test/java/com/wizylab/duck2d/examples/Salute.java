package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import java.awt.*;

import static com.wizylab.duck2d.Game.RANDOM;
import static com.wizylab.duck2d.Keyboard.*;
import static java.awt.event.KeyEvent.*;

public class Salute implements View {
    private static final int ALPHA = 100, SIZE_MIN = 5, SIZE_MAX = 50, COUNT = 1000;
    private static final Color[] COLORS = new Color[]{
            new Color(200, 200, 50, ALPHA),
            new Color(4, 147, 226, ALPHA),
            new Color(129, 3, 224, ALPHA),
            new Color(234, 2, 2, ALPHA),
            new Color(3, 216, 162, ALPHA),
            new Color(233, 108, 2, ALPHA)};
    private int[][] data = new int[COUNT][4];
    private int time;

    public static void main(String[] args) {
        Game.start(new Salute());
    }

    private Salute() {
        reset();
    }

    @Override
    public void onTimer(long interval) {
        if (onKey(VK_ESCAPE)) System.exit(0);
        time += interval;
        if (time > 2000) {
            time = 0;
            reset();
        }
    }

    @Override
    public void onDraw(Graph g) {
        for (int[] p : data) {
            g.setFillColor(COLORS[p[3]]);
            g.fillCircle(p[0], p[1], p[2]);
        }
    }

    private void reset() {
        for (int i = 0; i < data.length; i++) {
            data[i][0] = RANDOM.nextInt(800 + 2 * SIZE_MAX) - SIZE_MAX;
            data[i][1] = RANDOM.nextInt(600 + 2 * SIZE_MAX) - SIZE_MAX;
            data[i][2] = SIZE_MIN + RANDOM.nextInt(SIZE_MAX - SIZE_MIN);
            data[i][3] = RANDOM.nextInt(COLORS.length);
        }
    }
}