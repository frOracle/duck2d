package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.*;

import java.awt.*;
import java.awt.event.KeyEvent;

public class FallingBirds implements View {
    private static final String TEXTURES[] = {"blue", "red", "yellow"};
    //    private static final double ANIMATION_SPEED = 0.3, ROTATE_SEED = 0.03, GRAVITY = 0.3F, FORCE = -7;
    private static final double ANIMATION_SPEED = 0.1, ROTATE_SEED = 0.01, GRAVITY = 0.005F, FORCE = -1;
    private Bird[] birds = new Bird[500];
    private long score = 0;
    private int r = 10;

    public static void main(String[] args) {
        Game.start(new FallingBirds());
    }

    private FallingBirds() {
        for (int i = 0; i < birds.length; i++) birds[i] = new Bird();
    }

    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);

        if (Keyboard.onKey(KeyEvent.VK_X)) r += 20;
        if (Keyboard.onKey(KeyEvent.VK_Z)) r -= 20;
        if (r < 10) r = 10;

        double x = Mouse.x(), y = Mouse.y();
        for (Bird bird : birds) {
            if (Math.sqrt(Math.pow(bird.x + 17 - x, 2) + Math.pow(bird.y + 12 - y, 2)) < r && !bird.visible) {
                bird.visible = true;
                score++;
            }
            bird.move();
        }
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("fbirds/bg", 0, 0, 800, 600);
        g.setColor(Color.WHITE);
        g.circle(Mouse.x(), Mouse.y(), r);
        for (Bird bird : birds) bird.draw(g);
        g.setColor(Color.YELLOW);
        g.setTextStyle(2, 3, 20);
        g.text(10, 25, "SCORE: " + score);
    }

    private class Bird {
        private boolean visible;
        private double x, y, angle, velocity, frame;
        private int type;

        private Bird() {
            reset();
        }

        private void move() {
            if (!visible) return;
            angle += ROTATE_SEED;

            frame += ANIMATION_SPEED;
            if (frame > 3) frame = 0;

            velocity = velocity + GRAVITY;
            y += velocity;
            if (y > 650) reset();
        }

        private void reset() {
            type = (int) (Math.random() * 3);
            x = Math.random() * 800 - 34;
            y = Math.random() * 600 - 24;
            velocity = FORCE;
            visible = false;
            frame = 0;
        }

        private void draw(Graph g) {
            if (visible) {
                g.putRotateImage("fbirds/bird/" + TEXTURES[type] + "/" + (int) frame, angle, x, y);
            } else {
                g.setFillColor(Color.YELLOW);
                g.fillCircle((int) x + 17, (int) y + 12, 3);
                g.setFillColor(Color.RED);
                g.fillCircle((int) x + 17, (int) y + 12, 2);
            }
        }
    }
}