package com.wizylab.duck2d.examples.menu.keyboard;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.Keyboard;
import com.wizylab.duck2d.View;
import com.wizylab.duck2d.shape.Rectangle;

import java.awt.*;
import java.awt.event.KeyEvent;

@SuppressWarnings("Duplicates")
public class MenuView implements View {
    private Rectangle[] buttons = new Rectangle[4];
    private String[] titles = {"PLAY", "SETTINGS", "ABOUT", "EXIT"};
    private int value = 0;

    public MenuView() {
        buttons[0] = new Rectangle(328, 300, 144, 44);
        buttons[1] = new Rectangle(328, 360, 144, 44);
        buttons[2] = new Rectangle(328, 420, 144, 44);
        buttons[3] = new Rectangle(328, 480, 144, 44);
    }

    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);
        if (Keyboard.onKey(KeyEvent.VK_UP)) value = (value == 0) ? buttons.length - 1 : value - 1;
        if (Keyboard.onKey(KeyEvent.VK_DOWN)) value = (value == buttons.length - 1) ? 0 : value + 1;
        if (Keyboard.onKey(KeyEvent.VK_ENTER)) {
            if (value == 0) Game.show(GameView.class);
            if (value == 1) Game.show(SettingsView.class);
            if (value == 2) Game.show(AboutView.class);
            if (value == 3) System.exit(0);
        }
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("raptor/bg", 0, 0, 800, 600);
        g.setColor(Color.WHITE);
        g.setTextStyle(2, 1, 20);
        for (int i = 0; i < buttons.length; i++) {
            Rectangle b = buttons[i];
            String tx = "buttons/4/" + ((i == value) ? 1 : 0);
            g.putImage(tx, b.x, b.y);
            g.ctext(b, titles[i]);
        }
    }
}