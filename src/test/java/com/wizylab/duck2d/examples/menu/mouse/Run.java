package com.wizylab.duck2d.examples.menu.mouse;

import com.wizylab.duck2d.*;
import com.wizylab.duck2d.shape.Rectangle;

import java.awt.*;

@SuppressWarnings("Duplicates")
public class Run implements View {
    private Rectangle[] buttons = new Rectangle[4];
    private String[] titles = {"PLAY", "SETTINGS", "ABOUT", "EXIT"};
    private int value = -1;

    public static void main(String[] args) {
        Game.start(new Run());
    }

    private Run() {
        buttons[0] = new Rectangle(324, 300, 152, 47);
        buttons[1] = new Rectangle(324, 360, 152, 47);
        buttons[2] = new Rectangle(324, 420, 152, 47);
        buttons[3] = new Rectangle(324, 480, 152, 47);
    }

    @Override
    public void onTimer(long l) {
        value = -1;
        for (int i = 0; i < buttons.length; i++)
            if (buttons[i].contains(Mouse.x(), Mouse.y())) value = i;

        if (Mouse.onClick(MouseButton.LEFT)) {
            if (value == 0) System.out.println("GAME");
            if (value == 1) System.out.println("SETTINGS");
            if (value == 2) System.out.println("ABOUT");
            if (value == 3) System.exit(0);
        }
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("raptor/bg", 0, 0, 800, 600);

        g.setColor(Color.WHITE);
        g.setTextStyle(2, 1, 20);
        for (int i = 0; i < buttons.length; i++) {
            Rectangle b = buttons[i];
            String tx = "buttons/1/" + ((i == value) ? 1 : 0);
            g.putImage(tx, b.x, b.y);
            g.ctext(b, titles[i]);
        }
    }
}