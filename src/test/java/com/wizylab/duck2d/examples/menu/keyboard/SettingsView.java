package com.wizylab.duck2d.examples.menu.keyboard;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import java.awt.*;

import static com.wizylab.duck2d.Keyboard.onKey;
import static java.awt.event.KeyEvent.VK_ESCAPE;

public class SettingsView implements View {
    @Override
    public void onTimer(long l) {
        if (onKey(VK_ESCAPE)) Game.show(MenuView.class);
    }

    @Override
    public void onDraw(Graph g) {
        g.setColor(Color.WHITE);
        g.text(100,100,"SETTINGS");
    }
}