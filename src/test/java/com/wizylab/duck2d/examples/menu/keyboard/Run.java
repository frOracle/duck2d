package com.wizylab.duck2d.examples.menu.keyboard;

import com.wizylab.duck2d.Game;

public class Run {
    public static void main(String[] args) {
        Game.addView(new MenuView(), new GameView(), new SettingsView(), new AboutView());
        Game.start(MenuView.class);
    }
}