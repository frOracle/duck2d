package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.Mouse;
import com.wizylab.duck2d.View;

import static com.wizylab.duck2d.Keyboard.onKey;
import static java.awt.event.KeyEvent.VK_ESCAPE;

public class Rotate2 implements View {
    private static final float SPEED = 0.2F;
    private double x = 100, y = 100, angle = 0;

    public static void main(String[] args) {
        Game.start(new Rotate2());
    }

    @Override
    public void onTimer(long interval) {
        if (onKey(VK_ESCAPE)) System.exit(0);
        // смащаем
        double x = this.x - Mouse.x();
        double y = this.y - Mouse.y();
        // в цилиндрические
        double distance = Math.sqrt(x * x + y * y);
        double angle = Math.atan2(y, x);
        // перемещаем
        distance -= SPEED * interval;
        if (distance < 1) return;
        // переводим обратно
        x = distance * Math.cos(angle);
        y = distance * Math.sin(angle);
        // устанавливаем новые значения
        this.angle = angle - Math.PI / 2;
        this.x = x + Mouse.x();
        this.y = y + Mouse.y();
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("raptor/bg", 0, 0, 800, 600);
        g.putRotateImage("raptor/ships/9", angle, x - 25, y - 25, 50);
    }
}