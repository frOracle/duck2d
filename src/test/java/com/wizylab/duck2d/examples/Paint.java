package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.*;

import java.awt.*;
import java.awt.event.KeyEvent;

import static com.wizylab.duck2d.Keyboard.*;
import static java.awt.event.KeyEvent.*;

public class Paint implements View {
    private Graph g = new Graph(800, 600);
    private Color color = Color.BLUE;
    private int r = 5;

    public static void main(String[] args) {
        Game.start(new Paint());
    }

    @Override
    public void onTimer(long interval) {
        if (onKey(VK_ESCAPE)) System.exit(0);

        if (Keyboard.onKey(KeyEvent.VK_X)) r += 5;
        if (Keyboard.onKey(KeyEvent.VK_Z)) r -= 5;
        if (r < 5) r = 5;

        if (onKey(VK_1)) color = Color.RED;
        if (onKey(VK_2)) color = Color.ORANGE;
        if (onKey(VK_3)) color = Color.YELLOW;
        if (onKey(VK_4)) color = Color.GREEN;
        if (onKey(VK_5)) color = Color.BLUE;

        if (Mouse.hasClick(MouseButton.LEFT)) {
            g.setFillColor(color);
            g.fillCircle(Mouse.x(), Mouse.y(), r);
        }
    }

    @Override
    public void onDraw(Graph g) {
        g.setBackground(Color.BLACK);
        g.putGraph(this.g);
    }
}