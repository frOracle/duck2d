package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.*;

import java.awt.*;

import static com.wizylab.duck2d.Keyboard.hasKey;
import static com.wizylab.duck2d.Keyboard.onKey;
import static com.wizylab.duck2d.Mouse.hasClick;
import static com.wizylab.duck2d.Mouse.onClick;
import static java.awt.event.KeyEvent.*;

public class Colobok implements View {
    private float x = 100, y = 100;

    public static void main(String[] args) {
        Game.start(new Colobok());
    }

    @Override
    public void onTimer(long interval) {
        float speed = interval * 0.3F;
        if (onKey(VK_ESCAPE)) System.exit(0);
        if (hasKey(VK_LEFT)) x -= speed;
        if (hasKey(VK_UP)) y -= speed;
        if (hasKey(VK_RIGHT)) x += speed;
        if (hasKey(VK_DOWN)) y += speed;

        if (onClick(MouseButton.LEFT))
            System.out.println("MOUSE LEFT:" + Mouse.x() + " - " + Mouse.y());
        if (onClick(MouseButton.CENTER)) System.out.println("MOUSE CENTER");
        if (hasClick(MouseButton.RIGHT))
            System.out.println("MOUSE RIGHT:" + Mouse.x() + " - " + Mouse.y());
    }

    @Override
    public void onDraw(Graph g) {
        g.setBackground(Color.BLACK);
        g.setFillColor(Color.YELLOW);
        g.fillCircle((int) x, (int) y, 15);
    }
}