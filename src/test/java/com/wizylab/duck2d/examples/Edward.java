package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import static com.wizylab.duck2d.Game.RANDOM;
import static com.wizylab.duck2d.Keyboard.*;
import static java.awt.event.KeyEvent.*;

public class Edward implements View {
    private static final int[] DX = {100, 100, 400, 400};
    private static final float SPEED = 0.2F;
    private float[] angles = {0, 0, 0, 0};
    private int[] models = {6, 7, 8, 9};
    private boolean move = true;
    private float dy = 0;

    public static void main(String[] args) {
        Game.start(new Edward());
    }

    @Override
    public void onTimer(long l) {
        if (onKey(VK_ESCAPE)) System.exit(0);
        if (onKey(VK_SPACE)) move = !move;
        if (onKey(VK_ENTER))
            for (int i = 0; i < 4; i++)
                models[i] = RANDOM.nextInt(11);

        if (!move) return;
        float speed = l * SPEED;
        for (int i = 0; i < 4; i++)
            angles[i] = (angles[i] + SPEED < 360) ? angles[i] + speed : 0;
        dy = (dy + SPEED < 600) ? dy + speed : -300;
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("edward/bg", 0, 0, 800, 600);
        for (int i = 0; i < 4; i++) {
            int x = 80 + (int) (Math.sin(Math.PI / 180 * angles[i]) * 90), row = i % 2;
            g.putImage("edward/ships/" + models[i], DX[i] + x, dy + row * 150 + 50);
        }
    }
}