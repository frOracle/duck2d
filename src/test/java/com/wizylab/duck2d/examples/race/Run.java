package com.wizylab.duck2d.examples.race;

import com.wizylab.duck2d.Game;

public class Run {
    public static void main(String[] args) {
        Game.addView(new GameView(), new FinishView());
        Game.start(GameView.class);
    }
}