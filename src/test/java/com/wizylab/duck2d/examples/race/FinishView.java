package com.wizylab.duck2d.examples.race;

import com.wizylab.duck2d.Environment;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import java.awt.event.KeyEvent;

import static com.wizylab.duck2d.Keyboard.onKey;

public class FinishView implements View {

    @Override
    public void onTimer(long l) {
        if (onKey(KeyEvent.VK_ESCAPE)) System.exit(0);
    }

    @Override
    public void onDraw(Graph g) {
        int winer = Environment.get("winer", 0);
        g.setColor(GameView.COLORS[winer]);
        g.setTextStyle(1, 2, 100);
        g.text(120, 300, "YOU WIN!");
    }
}