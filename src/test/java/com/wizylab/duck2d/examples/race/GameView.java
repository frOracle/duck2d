package com.wizylab.duck2d.examples.race;

import com.wizylab.duck2d.Environment;
import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import java.awt.*;

import static com.wizylab.duck2d.Keyboard.hasKey;
import static com.wizylab.duck2d.Keyboard.onKey;
import static java.awt.event.KeyEvent.*;

public class GameView implements View {
    public static final Color[] COLORS = {Color.RED, Color.YELLOW, Color.GREEN, Color.BLUE};
    private static final int[] KEYS = {VK_ENTER, VK_SPACE, VK_A, VK_9};
    private static final float SPEED = 0.1f;
    private Light light;
    private float[] pos = {0, 0, 0, 0};

    public GameView() {
        light = new Light(620, 30, 500);
    }

    @Override
    public void onTimer(long l) {
        if (onKey(VK_ESCAPE)) System.exit(0);
        for (int i = 0; i < KEYS.length; i++) {
            boolean active = hasKey(KEYS[i]);
            if (!active) continue;
            pos[i] += SPEED;
            if (light.isRed()) pos[i] = 0;
            if (pos[i] > 700) {
                Environment.put("winer", i);
                Game.show(FinishView.class);
            }
        }
        light.onTimer(l);
    }

    @Override
    public void onDraw(Graph g) {
        light.onDraw(g);

        for (int i = 0; i < pos.length; i++) {
            g.setFillColor(COLORS[i]);
            g.fillRect(50 + (int) pos[i], 250 + i * 70, 50 + (int) pos[i] + 100, 250 + i * 70 + 50);
        }

        g.setColor(Color.GREEN);
        g.line(700, 0, 700, 600);
    }
}