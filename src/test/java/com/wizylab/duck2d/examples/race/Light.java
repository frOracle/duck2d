package com.wizylab.duck2d.examples.race;

import com.wizylab.duck2d.Graph;

import java.awt.*;

public class Light {
    private int x, y;
    private long value = 0;
    private int interval;

    public Light(int x, int y, int interval) {
        this.x = x;
        this.y = y;
        this.interval = interval;
    }

    public void onTimer(long l) {
        value += l;
        if (value / interval > 3) value = 0;
    }

    public boolean isRed() {
        return value / interval == 0;
    }

    public void onDraw(Graph g) {
        g.setFillColor(Color.DARK_GRAY);
        g.fillRect(x, y, x + 50, y + 140);

        g.setFillColor(Color.GRAY);
        g.fillCircle(x + 25, y + 25, 20);
        g.fillCircle(x + 25, y + 70, 20);
        g.fillCircle(x + 25, y + 115, 20);

        int value = (int) (this.value / interval);
        if (value == 0) {
            g.setFillColor(Color.RED);
            g.fillCircle(x + 25, y + 25, 20);
        } else if (value == 1 || value == 3) {
            g.setFillColor(Color.YELLOW);
            g.fillCircle(x + 25, y + 70, 20);
        } else if (value == 2) {
            g.setFillColor(Color.GREEN);
            g.fillCircle(x + 25, y + 115, 20);
        }
    }
}