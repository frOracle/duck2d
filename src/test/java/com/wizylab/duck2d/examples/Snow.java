package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import static com.wizylab.duck2d.Game.RANDOM;
import static com.wizylab.duck2d.Keyboard.*;
import static java.awt.event.KeyEvent.*;

public class Snow implements View {
    private float[][] data = new float[500][4];

    public static void main(String[] args) {
        Game.start(new Snow());
    }

    private Snow() {
        for (int i = 0; i < data.length; i++) restart(i, true);
    }

    @Override
    public void onTimer(long interval) {
        if (onKey(VK_ESCAPE)) System.exit(0);
        float k = (hasKey(VK_SPACE)) ? 1 : 10;
        for (int i = 0; i < data.length; i++) {
            data[i][1] += data[i][3] * interval / k;
            if (data[i][1] > 600) restart(i, false);
        }
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("snow/bg", 0, 0, 800, 600);
        for (float[] p : data) g.putImage("snow/ball", p[0], p[1], (int) p[2]);
    }

    private void restart(int i, boolean first) {
        data[i][0] = RANDOM.nextInt(800);
        data[i][1] = (first) ? RANDOM.nextInt(600) : -20 - RANDOM.nextInt(80);
        data[i][2] = 20 + RANDOM.nextInt(10);
        data[i][3] = (5F + RANDOM.nextInt(20)) / 25;
    }
}