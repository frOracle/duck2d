package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.Environment;
import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import java.awt.*;

import static com.wizylab.duck2d.Game.RANDOM;
import static com.wizylab.duck2d.Keyboard.*;
import static java.awt.event.KeyEvent.*;

public class Flappy implements View {
    private static final int PIPE_WIDTH = 52, PIPE_HEIGHT = 320, PIPE_MIN_HDIST = 200, PIPE_MIN_VDIST = 150;
    private static final float GRAVITY = 0.003F, POWER = 0.4F, SPEED = 0.15F, FSPEED = 0.01F;
    private static final String BG_TYPE = "day", BIRD_TYPE = "blue", PIPE_TYPE = "green";
    private static final int WIDTH = 288, HEIGHT = 512, X = 50;
    private float y, velocity, frame, bx = 0;
    private float[][] data = new float[5][4];
    private boolean finish = true;
    private long time = 0;

    public static void main(String[] args) {
        Environment.put("window.title", "MyFlappy");
        Environment.put("window.width", WIDTH);
        Environment.put("window.height", HEIGHT);
        Game.start(new Flappy());
    }

    private Flappy() {
        reset();
    }

    @Override
    public void onTimer(long interval) {
        if (onKey(VK_ESCAPE)) System.exit(0);
        if (finish && onKey(VK_ENTER)) {
            finish = false;
            reset();
        }
        if (finish) return;
        float difficult = time / 1000;
        float speed = (SPEED + difficult / 100 / 2) * interval;
        // add time
        time += interval;
        // change player frame
        float fspeed = interval * FSPEED;
        frame = (frame + fspeed < 4) ? frame + fspeed : 0;
        // move player
        velocity = (onKey(VK_SPACE)) ? -POWER : velocity + GRAVITY;
        y += velocity * interval;
        // move base
        bx -= speed;
        if (bx < -336) bx = 0;
        // move pipes
        for (int i = 0; i < data.length; i++) {
            data[i][0] -= speed;
            if (data[i][0] < -PIPE_WIDTH) resetPipe(i, data.length * PIPE_MIN_HDIST - PIPE_WIDTH);
        }
        if (crash()) finish = true;
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("flappy/background/" + BG_TYPE, 0, 0);
        if (finish) {
            g.putImage("flappy/base", 0, 400);
            g.putImage("flappy/message", 52, 66);
        } else {
            // draw pipes
            for (float[] p : data)
                for (int i = 0; i < 2; i++) {
                    String tx = String.format("flappy/pipe/%s/%d", PIPE_TYPE, i);
                    int y = (i == 0) ? (int) p[1] : (int) p[2];
                    g.putImage(tx, p[0], y);
                }
            // draw base
            g.putImage("flappy/base", bx, 400);
            g.putImage("flappy/base", bx + 336, 400);
            // draw player
            g.putImage(String.format("flappy/bird/%s/%d", BIRD_TYPE, (int) frame), X, y);
        }
        // print score
        g.setTextStyle(7, 0, 20);
        g.setColor(Color.BLUE);
        g.text(95, 470, "SCORE - " + zero((int) (time / 1000), 3));
    }

    private void reset() {
        velocity = frame = time = 0;
        for (int i = 0; i < data.length; i++)
            resetPipe(i, WIDTH + i * PIPE_MIN_HDIST);
        y = 100;
    }

    private void resetPipe(int i, int x) {
        data[i][0] = x;
        while (true) {
            data[i][1] = -RANDOM.nextInt(PIPE_HEIGHT - 30);
            data[i][2] = RANDOM.nextInt(400 - 30);
            float d = data[i][2] - (data[i][1] + PIPE_HEIGHT);
            float dist = PIPE_MIN_VDIST - time / 1000;
            if (dist < 50) dist = 50;
            if (d > dist) break;
        }
    }

    private boolean crash() {
        for (float[] p : data) {
            for (int j = 1; j < 3; j++) {
                int x1 = (int) p[0], x2 = x1 + 52;
                int y1 = (int) p[j], y2 = y1 + 320;
                for (int l = 0; l < 4; l++)
                    for (int k = 0; k < 4; k++) {
                        int xx = X + k * 32 / 4, yy = (int) (y + l * 24 / 4);
                        if (x1 <= xx && xx <= x2 && y1 <= yy && yy <= y2) return true;
                    }
            }
        }
        return (y <= 0 || y + 24 >= 400);
    }

    private static String zero(int value, int count) {
        StringBuilder sb = new StringBuilder(String.valueOf(value));
        for (int i = sb.length(); i < count; i++) sb.insert(0, "0");
        return sb.toString();
    }
}