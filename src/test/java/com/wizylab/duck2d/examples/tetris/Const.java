package com.wizylab.duck2d.examples.tetris;

import java.awt.*;

public class Const {
    public static final Color COLOR_TEXT = new Color(255, 255, 255);
    public static final Color COLOR_FIELD_BORDER = new Color(20, 29, 48);
    public static final Color COLOR_FIELD_BG = new Color(28, 43, 62);

    public static final Color[] COLORS = new Color[]{
            new Color(45, 57, 71),
            new Color(114, 229, 4),
            new Color(4, 147, 226),
            new Color(129, 3, 224),
            new Color(234, 2, 2),
            new Color(3, 216, 162),
            new Color(233, 108, 2)};

    public static final Point[][][] FIGURES = {
            // FIGURE I
            new Point[][]{
                    {new Point(-1, 0), new Point(0, 0), new Point(1, 0), new Point(2, 0)},
                    {new Point(1, -1), new Point(1, 0), new Point(1, 1), new Point(1, 2)}},
            // FIGURE J
            new Point[][]{
                    {new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(0, 0)},
                    {new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(2, 0)},
                    {new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(2, 2)},
                    {new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(0, 2)}},
            // FIGURE L
            new Point[][]{
                    {new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(2, 0)},
                    {new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(2, 2)},
                    {new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(0, 2)},
                    {new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(0, 0)}},
            // FIGURE O
            new Point[][]{
                    {new Point(0, 0), new Point(0, 1), new Point(1, 0), new Point(1, 1)}},
            // FIGURE S
            new Point[][]{
                    {new Point(1, 0), new Point(2, 0), new Point(0, 1), new Point(1, 1)},
                    {new Point(0, 0), new Point(0, 1), new Point(1, 1), new Point(1, 2)}},
            // FIGURE T
            new Point[][]{
                    {new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(1, 0)},
                    {new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(2, 1)},
                    {new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(1, 2)},
                    {new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(0, 1)}},
            // FIGURE Z
            new Point[][]{
                    {new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(2, 1)},
                    {new Point(1, 0), new Point(0, 1), new Point(1, 1), new Point(0, 2)}}
    };
}