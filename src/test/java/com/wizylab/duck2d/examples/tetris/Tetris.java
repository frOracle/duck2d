package com.wizylab.duck2d.examples.tetris;

import com.wizylab.duck2d.Environment;
import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import java.awt.*;

import static com.wizylab.duck2d.Game.RANDOM;
import static com.wizylab.duck2d.Keyboard.onKey;
import static com.wizylab.duck2d.examples.tetris.Const.*;
import static java.awt.event.KeyEvent.*;

public class Tetris implements View {
    private static final int MAX_TIME = 999000;
    private static final int INTERVAL = 1000;
    private static final int W = 10, H = 20;
    private static final int CELL_SIZE = 25;
    private int[][] map = new int[W][H];
    private int cShape, cColor, nShape, nColor;
    private int score, interval, time, prev;
    private int x, y, angle;
    private boolean finish;

    public static void main(String[] args) {
        Environment.put("window.title", "TETRIS");
        Environment.put("window.width", 400);
        Environment.put("window.height", 520);
        Game.start(new Tetris());
    }

    private Tetris() {
        reset().next().next();
    }

    @Override
    public void onTimer(long interval) {
        if (onKey(VK_ESCAPE)) System.exit(0);
        if (finish && onKey(VK_ENTER)) reset().next().next();
        if (onKey(VK_UP)) doAction(x, y, (angle + 1 < Const.FIGURES[cShape].length) ? angle + 1 : 0);
        if (onKey(VK_LEFT)) doAction(x - 1, y, angle);
        if (onKey(VK_RIGHT)) doAction(x + 1, y, angle);
        if (onKey(VK_DOWN)) doAction(x, y + 1, angle);

        if (finish) return;
        time += interval;
        if (time > MAX_TIME) time = MAX_TIME;
        if (time / this.interval == prev) return;
        prev = time / this.interval;
        doAction(x, y + 1, angle);
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("tetris/bg", 0, 0);
        // print result
        if (finish) {
            g.setColor(Color.RED);
            g.setTextStyle(7, 1, 70);
            g.text(27, 180, "GAME OVER");

            g.setTextStyle(13, 1, 30);
            g.setColor(Color.BLUE);
            g.text(100, 280, "YOU SCORE");
            g.text(165, 320, zero(score, 3));

            g.setColor(Color.RED);
            g.setTextStyle(7, 1, 14);
            g.text(120, 400, "PRESS ENTER TO RESTART");
            return;
        }

        // draw field
        g.setFillColor(COLOR_FIELD_BORDER);
        g.fillRect(8, 8, 12 + W * CELL_SIZE, 12 + H * CELL_SIZE, CELL_SIZE / 4);
        g.setFillColor(Const.COLOR_FIELD_BG);
        g.fillRect(9, 9, 11 + W * CELL_SIZE, 11 + H * CELL_SIZE, CELL_SIZE / 4);
        for (int y = 0; y < map[0].length; y++)
            for (int x = 0; x < map.length; x++)
                cell(g, x, y, map[x][y]);
        for (Point p : Const.FIGURES[cShape][angle]) {
            int x = p.x + this.x, y = p.y + this.y;
            if (x >= 0 && x < W && y >= 0 && y < H) cell(g, x, y, cColor);
        }

        // draw next figure
        g.setFillColor(COLOR_FIELD_BORDER);
        g.fillRect(270, 72, 390, 172, CELL_SIZE / 4);
        g.setFillColor(COLOR_FIELD_BG);
        g.fillRect(271, 73, 389, 171, CELL_SIZE / 4);
        g.setFillColor(COLORS[0]);
        g.fillRect(276, 78, 384, 166, CELL_SIZE / 4);
        // calculate figure size then find out gap
        int mx1 = W, mx2 = 0, my1 = H, my2 = 0;
        for (Point p : FIGURES[nShape][0]) {
            if (p.x < mx1) mx1 = p.x;
            if (p.x > mx2) mx2 = p.x;
            if (p.y < my1) my1 = p.y;
            if (p.y > my2) my2 = p.y;
        }
        int size = 23;
        int w = mx2 - mx1 + 1, h = my2 - my1 + 1;
        int dx = (384 - 276 - (w * size)) / 2;
        int dy = (166 - 78 - (h * size)) / 2;
        // draw figure
        g.setFillColor(COLORS[nColor]);
        for (Point p : FIGURES[nShape][0])
            g.fillRect(276 + dx + (p.x + Math.abs(mx1)) * size + 1, 78 + dy + (p.y + Math.abs(my1)) * size + 1,
                    276 + dx + (p.x + Math.abs(mx1) + 1) * size - 1, 78 + dy + (p.y + Math.abs(my1) + 1) * size - 1,
                    size / 4);

        // draw text panel
        g.setFillColor(COLOR_FIELD_BORDER);
        g.fillRect(270, 8, 390, 62, CELL_SIZE / 4);
        g.setFillColor(COLOR_FIELD_BG);
        g.fillRect(271, 9, 389, 61, CELL_SIZE / 4);
        g.setColor(COLOR_TEXT);
        g.setTextStyle(1, Font.BOLD, 12);
        g.text(298, 30, "TIME: " + zero(time / 1000, 3));
        g.text(285, 50, "SCORE: " + zero(score, 3));
    }

    private void doAction(int x, int y, int angle) {
        boolean stop = false;
        for (Point p : FIGURES[cShape][angle]) {
            int xx = p.x + x, yy = p.y + y;
            if (xx < 0 || xx >= W) return;
            if ((angle != this.angle || x != this.x) && (yy >= H || (yy >= 0 && map[xx][yy] != 0))) return;
            if (yy >= H || (yy >= 0 && map[xx][yy] != 0)) stop = true;
        }
        if (!stop) {
            this.x = x;
            this.y = y;
            this.angle = angle;
        } else push().clean().recalc().next();
    }

    private Tetris clean() {
        // prepare new map
        final int[][] res = new int[W][H];
        for (int y = 0; y < H; y++)
            for (int x = 0; x < W; x++) res[x][y] = 0;

        int j = H - 1;
        for (int y = H - 1; y >= 0; y--) {
            boolean remove = true;
            for (int x = 0; x < W; x++)
                if (map[x][y] == 0) remove = false;
            if (!remove) {
                for (int x = 0; x < W; x++) res[x][j] = map[x][y];
                j--;
            } else score++;
        }
        map = res;
        return this;
    }

    private Tetris push() {
        for (Point p : FIGURES[cShape][angle]) {
            int x = p.x + this.x, y = p.y + this.y;
            if (x >= 0 && x < W && y >= 0 && y < H) map[x][y] = cColor;
            if (y < 0) finish = true;
        }
        return this;
    }

    private Tetris reset() {
        for (int y = 0; y < H; y++)
            for (int x = 0; x < W; x++) map[x][y] = 0;
        prev = time = score = 0;
        interval = INTERVAL;
        finish = false;
        return this;
    }

    private Tetris next() {
        cColor = nColor;
        cShape = nShape;
        nColor = 1 + RANDOM.nextInt(COLORS.length - 1);
        nShape = RANDOM.nextInt(FIGURES.length);
        angle = 0;
        x = 4;
        y = -1;
        return this;
    }

    private Tetris recalc() {
        int speed = 20 * score;
        if (speed > INTERVAL - 1) speed = INTERVAL - 1;
        interval = INTERVAL - speed;
        return this;
    }

    private static void cell(Graph g, int x, int y, int color) {
        g.setFillColor(COLORS[color]);
        g.fillRect(10 + x * CELL_SIZE + 1, 10 + y * CELL_SIZE + 1,
                10 + (x + 1) * CELL_SIZE - 1, 10 + (y + 1) * CELL_SIZE - 1,
                CELL_SIZE / 4);
    }

    private static String zero(int value, int count) {
        StringBuilder sb = new StringBuilder(String.valueOf(value));
        for (int i = sb.length(); i < count; i++) sb.insert(0, "0");
        return sb.toString();
    }
}