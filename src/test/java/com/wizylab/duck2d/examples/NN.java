package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.Environment;
import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import java.awt.*;

import static com.wizylab.duck2d.Keyboard.hasKey;
import static com.wizylab.duck2d.Keyboard.onKey;
import static java.awt.event.KeyEvent.*;

public class NN implements View {
    private static final int START = 1, MAX_LEVEL = 200, WIN_LEVEL = 200;
    private boolean finish = false;
    private float value = START;
    private int level = 1;

    public static void main(String[] args) {
        Environment.put("window.title", "99");
        Game.start(new NN());
    }

    @Override
    public void onTimer(long l) {
        if (onKey(VK_ESCAPE)) System.exit(0);
        if (onKey(VK_SPACE)) {
            if ((int) value == 99) level++;
            if (level >= WIN_LEVEL) finish = true;
            value = START;
        }
        if (finish) return;
        int k = (hasKey(VK_SHIFT)) ? 150 : 0;
        int speed = MAX_LEVEL - level - k;
        if (speed < 1) speed = 1;
        value += (float) l / speed / 2;
        if (value >= 101) value = START;
    }

    @Override
    public void onDraw(Graph g) {
        if (finish) {
            g.setColor(Color.RED);
            g.setTextStyle(1, 1, 120);
            g.text(55, 320, "YOU WIN!");
        } else {
            g.setColor(Color.WHITE);
            g.setTextStyle(13, 1, 150);
            g.text(250, 320, zero((int) value, 3));
            g.setTextStyle(7, 1, 20);
            g.text(695, 27, "LEVEL - " + zero(level, 3));
        }
    }

    private static String zero(int value, int count) {
        StringBuilder sb = new StringBuilder(String.valueOf(value));
        for (int i = sb.length(); i < count; i++) sb.insert(0, "0");
        return sb.toString();
    }
}