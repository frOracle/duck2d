package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

public class Test implements View {
    public static void main(String[] args) {
        Game.start(new Test());
    }

    @Override
    public void onTimer(long interval) {

    }

    @Override
    public void onDraw(Graph g) {

    }
}
