package com.wizylab.duck2d.examples.ball.objects;

import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.examples.ball.GObject;
import com.wizylab.duck2d.examples.ball.Run;
import com.wizylab.duck2d.shape.Rectangle;

import static com.wizylab.duck2d.examples.ball.Run.SPEED;

public class Rabbit extends Rectangle implements GObject {
    private static final double ANIMATION_SPEED = SPEED / 20;
    private static final double MOTION_SPEED =  SPEED / 2;
    private Rectangle screen;
    private double frame = 0;

    public Rabbit(Run game, double x, double y) {
        super(x, y, 100, 60);
        screen = game.screen;
    }

    @Override
    public void onTimer(long l) {
        frame += ANIMATION_SPEED;
        if (frame > 5) frame = 0;
        x += MOTION_SPEED;
    }

    @Override
    public void onDraw(Graph g) {
        if (!this.intersects(screen)) return;
        int x = (int) (this.x - screen.x);
        int y = (int) (this.y - screen.y);
        g.putImage("ball/rabbit/" + (int) frame, x, y, (int) width, (int) height);
    }
}