package com.wizylab.duck2d.examples.ball.objects;

import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.examples.ball.GObject;
import com.wizylab.duck2d.examples.ball.Run;
import com.wizylab.duck2d.shape.Point;
import com.wizylab.duck2d.shape.Rectangle;

import static com.wizylab.duck2d.examples.ball.Run.GRAVITY;

public class Enemy extends Rectangle implements GObject {
    private static final int TYPE_VIKTIM = 0, TYPE_SPIKE = 1;
    private static final Point[] SIZE = new Point[]{
            new Point(156, 211),
            new Point(156, 263)
    };
    private boolean exist = true, death = false;
    private double sx, dist, velocity;
    private Rectangle screen;
    private double frame = 0;
    private int dir = 1;
    private int type;

    public Enemy(Run game, double x, double y, double dist, int type) {
        super(x, y, SIZE[type].x / 3, SIZE[type].y / 3);
        this.screen = game.screen;
        this.dist = dist;
        this.type = type;
        sx = x;
    }

    @Override
    public void onTimer(long l) {
        if (!exist) return;
        if (death) {
            velocity = velocity + GRAVITY;
            y += velocity;
            if (y > 6000) exist = false;
        } else {
            double dx = ((dir == 0) ? -1 : 1) * 0.04 * l;
            x += dx;
            if (dist > 0 && x > sx + dist) dir = 0;
            if (dist > 0 && x < sx) dir = 1;

            if (dist < 0 && x < sx + dist) dir = 1;
            if (dist < 0 && x > sx) dir = 0;
        }
        frame += 0.01 * l;
        if (!death && frame > 6) frame = 0;
        if (death && frame > 5) frame = 5;
    }

    @Override
    public boolean collision(Player player) {
        if (!exist || death || !this.intersects(player)) return false;
        if (type == 1) player.life--;
        velocity = -1.8;
        death = true;
        frame = 0;
        return true;
    }

    @Override
    public void onDraw(Graph g) {
        if (!exist || !this.intersects(screen)) return;
        int x = (int) (this.x - screen.x);
        int y = (int) (this.y - screen.y);
        int state = (death) ? 2 + dir : dir;
        String tx = "ball/enemy/" + type + "/" + state + "/" + (int) frame;
        g.putImage(tx, x, y, (int) width, (int) height);
    }
}