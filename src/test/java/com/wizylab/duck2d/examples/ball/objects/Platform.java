package com.wizylab.duck2d.examples.ball.objects;

import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.examples.ball.GObject;
import com.wizylab.duck2d.examples.ball.Run;
import com.wizylab.duck2d.shape.Point;
import com.wizylab.duck2d.shape.Rectangle;

public class Platform extends Rectangle implements GObject {
    private static final int TYPE_SMALL = 0, TYPE_BIG = 1;
    private static final Point[] SIZE = new Point[]{
            new Point(50, 16),
            new Point(90, 20)
    };
    private Rectangle screen;

    public Platform(Run game, double x, double y, int type) {
        super(x, y, SIZE[type].x, SIZE[type].y);
        screen = game.screen;
    }

    @Override
    public void onTimer(long l) {
    }

    @Override
    public boolean collision(Player player) {
        return this.intersects(player);
    }

    @Override
    public void onDraw(Graph g) {
        if (!this.intersects(screen)) return;
        int x = (int) (this.x - screen.x);
        int y = (int) (this.y - screen.y);
        g.putImage("ball/blocks/0", x, y, (int) width, (int) height);
    }
}