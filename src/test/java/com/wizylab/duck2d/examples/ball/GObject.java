package com.wizylab.duck2d.examples.ball;

import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.examples.ball.objects.Player;

public interface GObject {
    void onTimer(long l);

    void onDraw(Graph g);

    default boolean collision(Player player) {
        return false;
    }
}