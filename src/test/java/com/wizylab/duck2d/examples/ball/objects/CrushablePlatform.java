package com.wizylab.duck2d.examples.ball.objects;

import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.examples.ball.GObject;
import com.wizylab.duck2d.examples.ball.Run;
import com.wizylab.duck2d.shape.Point;
import com.wizylab.duck2d.shape.Rectangle;

public class CrushablePlatform extends Rectangle implements GObject {
    private static final int TYPE_SMALL = 0, TYPE_BIG = 1;
    private static final Point[] SIZE = new Point[]{
            new Point(50, 16),
            new Point(90, 20)
    };
    private static final int DURABILITY = 1;
    private boolean exist = true;
    private Rectangle screen;
    private int state = 0;

    public CrushablePlatform(Run game, double x, double y, int type) {
        super(x, y, SIZE[type].x, SIZE[type].y);
        screen = game.screen;
    }

    @Override
    public void onTimer(long l) {
    }

    @Override
    public boolean collision(Player player) {
        if (!exist || !this.intersects(player)) return false;
        if (player.y >= y) return true;
        state++;
        if (state > DURABILITY) exist = false;
        return true;
    }

    @Override
    public void onDraw(Graph g) {
        if (!exist || !this.intersects(screen)) return;
        int x = (int) (this.x - screen.x);
        int y = (int) (this.y - screen.y);
        g.putImage("ball/blocks/" + (1 + state), x, y, (int) width, (int) height);
    }
}