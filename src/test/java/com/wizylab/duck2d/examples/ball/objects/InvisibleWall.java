package com.wizylab.duck2d.examples.ball.objects;

import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.examples.ball.GObject;
import com.wizylab.duck2d.shape.Rectangle;

public class InvisibleWall extends Rectangle implements GObject {
    public InvisibleWall(double x, double y, double width, double height) {
        super(x, y, width, height);
    }

    @Override
    public void onTimer(long l) {
    }

    @Override
    public boolean collision(Player player) {
        return this.intersects(player);
    }

    @Override
    public void onDraw(Graph g) {
    }
}