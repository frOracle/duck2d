package com.wizylab.duck2d.examples.ball.objects;

import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.examples.ball.GObject;
import com.wizylab.duck2d.examples.ball.Run;
import com.wizylab.duck2d.shape.Circle;
import com.wizylab.duck2d.shape.Point;

import static com.wizylab.duck2d.examples.ball.Run.*;

public class Player extends Circle implements GObject {
    private static final Point START = new Point(250, 4500);
    private static final double ANIMATION_SPEED = SPEED / 2.5;
    public static final double MOTION_SPEED = SPEED / 3.3;
    public int size, score = 0, life = 3, jumpTime;
    public double velocity, angle, aDir;
    public boolean fly = false;
    private Run game;

    public Player(Run game) {
        super(0, 0, 20);
        this.game = game;
        restart();
    }

    public void restart() {
        x = START.x;
        y = START.y;
        game.screen.x = START.x - 100;
        game.screen.y = START.y;
        jumpTime = 0;
        velocity = 1;
        angle = 0;
        size = 20;
        aDir = 1;
    }

    public void up(double dy) {
        if (fly) y -= dy;
    }

    public void down(double dy) {
        if (fly) y += dy;
    }

    public void left(double dx) {
        aDir = -1;
        x -= dx;
        if (x < 20) x = 20;
    }

    public void right(double dx) {
        aDir = 1;
        x += dx;
    }

    @Override
    public void onTimer(long l) {
        angle += aDir * ANIMATION_SPEED * l;
        if (angle > 360) angle = angle - 360;
        if (angle < 0) angle = 360 - angle;

        if (fly) return;

        jumpTime += l;
        if (jumpTime > 100 && velocity == 0) {
            velocity -= POWER;
            jumpTime = jumpTime - 100;
        }

        velocity += GRAVITY;
        y += velocity * l;
    }

    @Override
    public void onDraw(Graph g) {
        int x = (int) (this.x - game.screen.x - size);
        int y = (int) (this.y - game.screen.y - size);
        double angle = Math.PI / 180 * this.angle;
        g.putRotateImage("ball/player", angle, x, y);
    }

    public boolean inWall() {
        for (GObject block : game.objects)
            if (block.collision(this)) return true;
        return false;
    }
}