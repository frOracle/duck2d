package com.wizylab.duck2d.examples.ball.objects;

import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.examples.ball.GObject;
import com.wizylab.duck2d.examples.ball.Run;
import com.wizylab.duck2d.shape.Rectangle;

public class Background implements GObject {
    private static final int WIDTH = 900, HEIGHT = 600;
    private Rectangle screen;
    private int type;

    public Background(Run game, int type) {
        screen = game.screen;
        this.type = type;
    }

    @Override
    public void onTimer(long l) {
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("ball/bg/" + type + "/0", 0, 0, WIDTH, HEIGHT);
        draw(g, "ball/bg/" + type + "/1", screen.x * 0.2, 0, 600);
        draw(g, "ball/bg/" + type + "/2", screen.x * 0.4, 50, 123);
        draw(g, "ball/bg/" + type + "/3", screen.x * 0.6, 4775 - screen.y, 226);
        draw(g, "ball/bg/" + type + "/4", screen.x * 1, 4981 - screen.y, 120);
    }

    private static void draw(Graph g, String tx, double x, double y, int height) {
        g.putImage(tx, -x % WIDTH, y, WIDTH, height);
        g.putImage(tx, -x % WIDTH + WIDTH, y, WIDTH, height);
    }
}