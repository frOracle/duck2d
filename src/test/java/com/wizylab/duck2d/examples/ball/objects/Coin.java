package com.wizylab.duck2d.examples.ball.objects;

import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.examples.ball.GObject;
import com.wizylab.duck2d.examples.ball.Run;
import com.wizylab.duck2d.shape.Circle;
import com.wizylab.duck2d.shape.Rectangle;

import static com.wizylab.duck2d.examples.ball.Run.SPEED;

public class Coin extends Circle implements GObject {
    private static final double ANIMATION_SPEED = SPEED / 100;
    private boolean exist = true;
    private double frame = 0;
    private Rectangle screen;

    public Coin(Run game, double x, double y) {
        super(x, y, 70 / 5);
        this.screen = game.screen;
    }

    @Override
    public void onTimer(long l) {
        frame += ANIMATION_SPEED * l;
        if (frame > 7) frame = 0;
    }

    @Override
    public boolean collision(Player player) {
        if (!exist || !this.intersects(player)) return false;
        player.score += 100;
        exist = false;
        return false;
    }

    @Override
    public void onDraw(Graph g) {
        if (!exist || !this.intersects(screen)) return;
        int x = (int) (this.x - screen.x);
        int y = (int) (this.y - screen.y);
        g.putImage("ball/coin/" + (int) frame, x - r, y - r, (int) (2 * r));
    }
}