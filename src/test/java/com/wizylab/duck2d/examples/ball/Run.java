package com.wizylab.duck2d.examples.ball;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;
import com.wizylab.duck2d.examples.ball.objects.*;
import com.wizylab.duck2d.shape.Rectangle;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import static com.wizylab.duck2d.Keyboard.hasKey;
import static com.wizylab.duck2d.Keyboard.onKey;
import static java.awt.event.KeyEvent.*;

public class Run implements View {
    public static final double GRAVITY = 0.02, POWER = 1.2, SPEED = 1F;
    private static final String FILENAME = "data/ball/map.txt";
    public static final double SCREEN_SPEED = SPEED / 2;
    public Rectangle screen = new Rectangle(0, 0, 800, 600);
    public ArrayList<GObject> objects = new ArrayList<>();
    public Player player = new Player(this);
    private Background bg;

    public static void main(String[] args) {
        Game.start(new Run());
    }

    @Override
    public void onShow() {
        reset();
    }

    public void reset() {
        objects.clear();
        try (Scanner sc = new Scanner(new File(FILENAME))) {
            bg = new Background(this, 0);
            while (sc.hasNextLine()) {
                int type = sc.nextInt();
                if (type == 0) objects.add(new InvisibleWall(sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt()));
                if (type == 1) objects.add(new Platform(this, sc.nextInt(), sc.nextInt(), sc.nextInt()));
                if (type == 2) objects.add(new CrushablePlatform(this, sc.nextInt(), sc.nextInt(), sc.nextInt()));
                if (type == 3) objects.add(new Coin(this, sc.nextInt(), sc.nextInt()));
                if (type == 4) objects.add(new Enemy(this, sc.nextInt(), sc.nextInt(), sc.nextInt(), 0));
                if (type == 5) objects.add(new Enemy(this, sc.nextInt(), sc.nextInt(), sc.nextInt(), 1));
                if (type == 98) objects.add(new Golem(this, sc.nextInt(), sc.nextInt(), sc.nextInt()));
                if (type == 99) objects.add(new Rabbit(this, sc.nextInt(), sc.nextInt()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        player = new Player(this);
    }

    @Override
    public void onTimer(long l) {
        if (onKey(VK_ESCAPE)) System.exit(0);

        if (!hasKey(VK_LEFT) && !hasKey(VK_RIGHT)) player.aDir = 0;
        if (onKey(VK_F)) player.fly = !player.fly;
        if (onKey(VK_R)) player.restart();

        double x = player.x, y = player.y;
        double move = Player.MOTION_SPEED * l * (player.fly ? 3 : 1);
        if (hasKey(VK_LEFT)) player.left(move);
        if (hasKey(VK_RIGHT)) player.right(move);
        if (hasKey(VK_DOWN)) player.down(move);
        if (hasKey(VK_UP)) player.up(move);

        player.onTimer(l);
        for (GObject obj : objects) obj.onTimer(l);

        if (player.inWall()) {
            player.velocity = 0;
            player.x = x;
            player.y = y;
        }

        if (player.life < 1) reset();

        if (player.x - screen.x < 200) screen.x -= move;
        if (player.x - screen.x > 600) screen.x += move;
        if (player.y - screen.y < 110)
            screen.y -= Math.max(SCREEN_SPEED * l, (player.fly) ? move : player.velocity * l);
        if (player.y - screen.y > 490)
            screen.y += Math.max(SCREEN_SPEED * l, (player.fly) ? move : player.velocity * l);
        if (screen.x < 0) screen.x = 0;
    }

    @Override
    public void onDraw(Graph g) {
        bg.onDraw(g);
        player.onDraw(g);
        for (GObject obj : objects) obj.onDraw(g);
        // draw interface
        g.setTextStyle(9, 2, 18);
        g.setColor(new Color(52, 69, 133));
        g.text(10, 24, "LIFE: " + player.life);
        g.text(100, 24, "SCORE: " + player.score);
    }
}