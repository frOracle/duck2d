package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import java.awt.*;

import static com.wizylab.duck2d.Game.RANDOM;
import static com.wizylab.duck2d.Keyboard.*;
import static java.awt.event.KeyEvent.*;

@SuppressWarnings("Duplicates")
public class Raptor implements View {
    private static final float SPEED = 0.3F, ROCKET_SPEED = 0.5F;
    private float[][] rocks = new float[30][6];
    private float[][] rockets = new float[30][2];
    private float x = (800F - 96F) / 2, y = 500;
    private int result = 0, life = 3, score = 0;

    public static void main(String[] args) {
        Game.start(new Raptor());
    }

    private Raptor() {
        for (int i = 0; i < rocks.length; i++) reset(i, true);
        for (int i = 0; i < rockets.length; i++) rockets[i][1] = -100;
    }

    @Override
    public void onTimer(long interval) {
        if (onKey(VK_ESCAPE)) System.exit(0);
        if (onKey(VK_R)) {
            x = (800F - 96F) / 2;
            y = 500;
        }
        if (result != 0) return;
        // move rocks
        for (int i = 0; i < rocks.length; i++) {
            float speed = interval * rocks[i][5];
            rocks[i][1] += speed;
            if (rocks[i][1] > 600) reset(i, false);
            rocks[i][3] += speed / 10;
            if (rocks[i][3] > 15) rocks[i][3] = 0;
            // player vs rock
            int x1 = (int) x, y1 = (int) y, x2 = x1 + 80, y2 = y1 + 80;
            if (into(i, x1, y1, x2, y2)) {
                life = (life - 1 >= 0) ? life - 1 : 0;
                if (life == 0) result = -1;
                reset(i, false);
            }
        }
        // move rockets
        for (int i = 0; i < rockets.length; i++) {
            if (rockets[i][1] < -20) continue;
            rockets[i][1] -= interval * ROCKET_SPEED;
            // rocket vs rock
            for (int j = 0; j < rocks.length; j++) {
                int x1 = (int) rockets[i][0], y1 = (int) rockets[i][1], x2 = x1 + 20, y2 = y1 + 20;
                if (!into(j, x1, y1, x2, y2)) continue;
                rockets[i][1] = -100;
                reset(j, false);
                if (++score >= 100) {
                    score = 100;
                    result = 1;
                }
                break;
            }
        }
        // move
        float speed = interval * SPEED;
        if (hasKey(VK_LEFT)) x -= speed;
        if (hasKey(VK_UP)) y -= speed;
        if (hasKey(VK_RIGHT)) x += speed;
        if (hasKey(VK_DOWN)) y += speed;
        if (onKey(VK_SPACE)) {
            for (int j = 0; j < 2; j++) {
                for (int i = 0; i < rockets.length; i++) {
                    if (rockets[i][1] > -20) continue;
                    rockets[i][0] = x + ((j == 0) ? -7 : 64);
                    rockets[i][1] = y + 15;
                    break;
                }
            }
        }
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("raptor/bg", 0, 0, 800, 600);
        for (float[] rock : rocks) {
            String tx = String.format("raptor/rocks/%d/%d", (int) rock[2], (int) rock[3]);
            g.putImage(tx, (int) rock[0], rock[1], (int) rock[4]);
        }
        for (float[] rocket : rockets)
            g.putImage("raptor/rockets/1", rocket[0], rocket[1], 20);
        g.putImage("raptor/ships/9", x, y, 80);
        // print score
        g.setTextStyle(7, 1, 20);
        g.setColor(Color.WHITE);
        g.text(10, 25, "SCORE: " + score);
        g.text(10, 48, "LIFE: " + life);
        // check end game
        if (result == 0) return;
        g.setTextStyle(0, 1, 80);
        if (result == -1) {
            g.setColor(Color.RED);
            g.text(150, 300, "GAME OVER");
        } else {
            g.setColor(Color.BLUE);
            g.text(200, 300, "YOU WIN!");
        }
    }

    private void reset(int i, boolean first) {
        rocks[i][0] = RANDOM.nextInt(800);                          // x
        rocks[i][1] = -RANDOM.nextInt((first) ? 600 : 100);                // y
        rocks[i][2] = RANDOM.nextInt(4);                            // type
        rocks[i][3] = RANDOM.nextInt(16);                           // frame
        rocks[i][4] = 32 + RANDOM.nextInt(40);                      // size
        rocks[i][5] = 0.05F + (float) RANDOM.nextInt(15) / 100;     // speed
    }

    private boolean into(int i, int x1, int y1, int x2, int y2) {
        for (int l = 0; l < 4; l++)
            for (int k = 0; k < 4; k++) {
                int xx = (int) (rocks[i][0] + k * rocks[i][4] / 4), yy = (int) (rocks[i][1] + l * rocks[i][4] / 4);
                if (x1 <= xx && xx <= x2 && y1 <= yy && yy <= y2) return true;
            }
        return false;
    }
}