package com.wizylab.duck2d.examples;

import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.View;

import static com.wizylab.duck2d.Keyboard.onKey;
import static java.awt.event.KeyEvent.VK_ESCAPE;

public class Rotate1 implements View {
    private float angle = 0;

    public static void main(String[] args) {
        Game.start(new Rotate1());
    }

    @Override
    public void onTimer(long interval) {
        if (onKey(VK_ESCAPE)) System.exit(0);

        angle += 0.01F;
        if (angle > 2 * Math.PI) angle = 0;
    }

    @Override
    public void onDraw(Graph g) {
        g.putRotateImage("raptor/ships/0", angle, 100, 100);
        g.putRotateImage("raptor/ships/0", angle, 250, 250, 100);
        g.putRotateImage("raptor/ships/0", angle, 400, 400, 80, 40);
    }
}